## IAS Hackthon 1 - Feb '16

Team Number: 
Member1: Gaurav Agarwal/20162088	
Member2: Syed Muzammil/20162119
Member3: Ramamurthi Kumar/20162029
--------------------------------


### Completed Tasks:
Succesfully set up tomcat.
setup command line apache kafka.


##partial completion
partially implemented kafka messaging system.


### Major challenges faced:
kafka messaging tool unable to set up properly


### Description of code: 

ServletEngine.java
	Receives requests from user, directs the requests in the form of messages to various message queus corresponding to the various services. Receives the response from these services using apache kafka and sends the response to the user.

SimpleConsumer.java
	Suscribes to login topic. Any messages that come to its queue, takes the user-id and password and if the user is genuine, authenticates him/her successfully and sends response to the servlet engine the token generated for this particular user.

FileService.java
	deals with file read/write/update request from the user and sends the response.


## Individual Participant Contributions:
-------------------------------------
Member1: laid out the project plan , desgined the modules to be done.
Member2: implemented SimpleConsumer.java, file that authenticates and authorizes the users.
Member3: tried setting up the apache kafka system, worked in setting up the tomcat server, fileservice.java

